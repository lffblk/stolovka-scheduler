package com.nikitasex.stolovkascheduler;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestApplication.class}, webEnvironment = RANDOM_PORT)
public class BaseSpringTest {

    @Test
    @Ignore
    public void testNothing() {
        //Used to ignore "No runnable methods" error. The class is intended for inheritance
    }
}
