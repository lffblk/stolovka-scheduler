package com.nikitasex.stolovkascheduler.exception;

public class StolovkaShedulerException extends RuntimeException {
    public StolovkaShedulerException(String msg) {
        super(msg);
    }
}
