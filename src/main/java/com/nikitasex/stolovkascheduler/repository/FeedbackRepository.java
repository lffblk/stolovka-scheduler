package com.nikitasex.stolovkascheduler.repository;

import com.nikitasex.stolovkascheduler.model.Feedback;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;

/**
 * Mongo repository of feedbacks
 */
public interface FeedbackRepository extends MongoRepository<Feedback, String> {
    /**
     * Returns all Feedbacks of User with defined User Id
     *
     * @param userId User ID
     * @return user feedbacks
     */
    Collection<Feedback> findAllByUserId(String userId);

    /**
     * Returns all Feedbacks for Place with defined Place ID
     *
     * @param placeId Place ID
     * @return place feedbacks
     */
    Collection<Feedback> findAllByPlaceId(String placeId);

    /**
     * Returns Feedback of User with defined User ID for Place with defined Place ID
     *
     * @param userId  User ID
     * @param placeId Place ID
     * @return user Feedback for place
     */
    Feedback findByUserIdAndPlaceId(String userId, String placeId);
}
