package com.nikitasex.stolovkascheduler.repository;

import com.nikitasex.stolovkascheduler.model.Event;
import com.nikitasex.stolovkascheduler.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Collection;
import java.util.List;

/**
 * Mongo repository for Lunch events
 */
public interface EventRepository extends MongoRepository<Event, String> {
    @Query(value = "{\n" +
            "  '$and': [\n" +
            "    {\n" +
            "      'eventDate': {\n" +
            "        '$gte': '$currentDate'\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      '$or': [\n" +
            "        {\n" +
            "          'participants.userId': \"_param_0\"\n" +
            "        },\n" +
            "        {\n" +
            "          'invitedUsers.userId': \"_param_0\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}")
    List<Event> findAllActualEventsByUserId(String userId);

    @Query(value = "{ 'eventDate':{ '$gte': '$currentDate' }, 'participants.userId': ?0 }")
    List<Event> findAllAcceptedActualEventsByUserId(String userId);

    @Query(value = "{ 'eventDate':{ '$gte': '$currentDate' }, 'invitedUsers.userId': ?0 }")
    List<Event> findAllPendingActualEventsByUserId(String userId);
}
