package com.nikitasex.stolovkascheduler.repository;

import com.nikitasex.stolovkascheduler.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Mongo repository of users
 */
public interface UserRepository extends MongoRepository<User, String> {

    /**
     * Finds User by user name
     *
     * @param userName  Telegram username
     * @return found User
     */
    User findByUserName(String userName);

    /**
     * Finds User by first name and last name
     *
     * @param firstName  Telegram user first name
     * @param lastName  Telegram user last name
     * @return found User
     */
    User findByFirstNameAndLastName(String firstName, String lastName);
}
