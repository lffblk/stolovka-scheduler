package com.nikitasex.stolovkascheduler.repository;

import com.nikitasex.stolovkascheduler.model.Place;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Mongo repository of places
 */
public interface PlaceRepository extends MongoRepository<Place, String> {
}
