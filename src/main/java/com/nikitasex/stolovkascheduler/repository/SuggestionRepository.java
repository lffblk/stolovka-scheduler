package com.nikitasex.stolovkascheduler.repository;

import com.nikitasex.stolovkascheduler.model.Suggestion;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SuggestionRepository extends MongoRepository<Suggestion, String> {
}
