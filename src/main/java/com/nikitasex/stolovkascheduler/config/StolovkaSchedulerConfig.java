package com.nikitasex.stolovkascheduler.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
public class StolovkaSchedulerConfig {

    @Value("${telegram.proxy.host}")
    private String proxyHost;

    @Value("${telegram.proxy.port}")
    private int proxyPort;

    @Value("${telegram.date.pattern}")
    private String datePattern;

    @Bean
    public DefaultBotOptions defaultBotOptions() {
        DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);
        botOptions.setProxyType(DefaultBotOptions.ProxyType.SOCKS5);
        botOptions.setProxyHost(proxyHost);
        botOptions.setProxyPort(proxyPort);
        return botOptions;
    }

    @Bean
    public DateFormat dateFormat() {
        return new SimpleDateFormat(datePattern);
    }
}
