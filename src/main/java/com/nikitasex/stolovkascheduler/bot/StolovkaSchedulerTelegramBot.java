package com.nikitasex.stolovkascheduler.bot;

import com.nikitasex.stolovkascheduler.model.Action;
import com.nikitasex.stolovkascheduler.model.Event;
import com.nikitasex.stolovkascheduler.model.User;
import com.nikitasex.stolovkascheduler.service.UserService;
import com.nikitasex.stolovkascheduler.util.StaticBeanService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
@Service
public class StolovkaSchedulerTelegramBot extends TelegramLongPollingBot {

    @Value("${telegram.bot.username}")
    private String botUserName;

    @Value("${telegram.bot.token}")
    private String botToken;

    private final UserService userService;

    @Autowired
    public StolovkaSchedulerTelegramBot(final DefaultBotOptions options,
                                        final UserService userService) {
        super(options);
        this.userService = userService;
    }

    @Override
    public void onUpdateReceived(Update update) {
        log.debug("Update = " + update);
        Message message = update.getMessage();
        User user = getUser(message.getFrom());
        updateLocation(user, message);

        if (StringUtils.isBlank(message.getText())) {
            return;
        }

        String result = parseCommand(update);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(result);
        try {
            sendApiMethod(sendMessage);
        }
        catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return botUserName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    /**
     * Checks whether current Telegram user is registered in system. In case user not found, creates new user.
     *
     * @param tgUser  Telegram user
     * @return found or created User
     */
    private User getUser(org.telegram.telegrambots.meta.api.objects.User tgUser) {
        User user = userService.findUser(tgUser.getUserName(), tgUser.getFirstName(), tgUser.getLastName());
        if (user == null) {
            user = User.builder()
                    .userId(tgUser.getId().toString())
                    .userName(tgUser.getUserName())
                    .firstName(tgUser.getFirstName())
                    .lastName(tgUser.getLastName())
                    .build();
            userService.save(user);
        }
        return user;
    }

    private void updateLocation(User user, Message message) {
        if (message.getLocation() != null) {
            user.setLocation(message.getLocation());
            userService.save(user);
        }
    }

    private String parseCommand(Update update) {
        Message message = update.getMessage();
        User user = getUser(message.getFrom());

        String[] splitted = message.getText().split(" ");
        String command = splitted.length == 0 ? message.getText() : splitted[0];

        Action action = Action.getActionByCommand(command);
        String result = action.getResult(update, user);

        if (Action.INVITE.getCommand().equals(command)) {
            sendNotifications(update, result);
            return "Notifications were sent";
        }
        if (Action.SUGGEST.getCommand().equals(command)) {
            Event event = Action.getLastUserEventByNum(user, splitted[1]);

            for (User invitedUser : event.getInvitedUsers()) {
                sendMessageToUser(invitedUser.getUserId(), result);
            }

            for (User participant : event.getParticipants()) {
                sendMessageToUser(participant.getUserId(), result);
            }

            return "Notifications were sent";
        }
        return result;
    }

    private void sendNotifications(Update update, String message) {
        UserService userService = StaticBeanService.getBean(UserService.class);
        userService.getMentionedUserIds(update).forEach(id -> sendMessageToUser(id, message));
    }

    private void sendMessageToUser(String userId, String msg) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(userId);
        sendMessage.setText(msg);
        try {
            sendApiMethod(sendMessage);
        }
        catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
