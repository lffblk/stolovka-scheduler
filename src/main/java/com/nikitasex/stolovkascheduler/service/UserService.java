package com.nikitasex.stolovkascheduler.service;

import com.nikitasex.stolovkascheduler.model.User;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

/**
 * Provides methods to process Users.
 */
public interface UserService {

    /**
     * Returns User by user name
     *
     * @param userName  Telegram username
     * @param firstName  Telegram user first name
     * @param lastName  Telegram user last name
     * @return found User
     */
    User findUser(String userName, String firstName, String lastName);

    /**
     * Saves User in DB
     *
     * @param user  creating or updating user
     */
    void save(User user);


    /**
     * Returns User by user name
     *
     * @param update BotApiUpdate
     * @return list of user ids are mentioned in a message
     */
    List<String> getMentionedUserIds(Update update);
}
