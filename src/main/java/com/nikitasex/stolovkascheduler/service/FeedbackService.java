package com.nikitasex.stolovkascheduler.service;

import com.nikitasex.stolovkascheduler.model.Feedback;

import java.util.Collection;
import java.util.Optional;

/**
 * Provides methods to process User Feedback for Places
 */
public interface FeedbackService {
    /**
     * Creates (or modifies if it is already created) Feedback of User for specified Place
     *
     * @param userId       User ID
     * @param placeId      Place ID
     * @param feedbackText Feedback message
     * @return created (updated) Feedback
     */
    Feedback setFeedback(String userId, String placeId, String feedbackText);

    /**
     * Returns all Feedbacks of specified User
     *
     * @param userId user id
     * @return user feedbacks collection
     */
    Collection<Feedback> getFeedbacksByUserId(String userId);

    /**
     * Returns all Feedbacks for specified Place
     *
     * @param placeId place id
     * @return place feedbacks collection
     */
    Collection<Feedback> getFeedbacksByPlaceId(String placeId);

    /**
     * Returns feedback of specified User for specified Place
     *
     * @param userId  User Id
     * @param placeId Place Id
     * @return Feedback
     */
    Optional<Feedback> getFeedbackByUserAndPlaceId(String userId, String placeId);
}
