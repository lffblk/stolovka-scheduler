package com.nikitasex.stolovkascheduler.service.impl;

import com.nikitasex.stolovkascheduler.model.Event;
import com.nikitasex.stolovkascheduler.model.Place;
import com.nikitasex.stolovkascheduler.model.User;
import com.nikitasex.stolovkascheduler.repository.EventRepository;
import com.nikitasex.stolovkascheduler.service.EventService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Default implementation of Event Service
 */
@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultEventService implements EventService {
    private EventRepository eventRepository;

    @Override
    public Event createEvent(User creator, Place place, Date startDate, String message) {
        Event event = Event.builder()
                .creator(creator)
                .place(place)
                .eventDate(startDate)
                .creationDate(new Date())
                .message(message)
                .build();
        event.addParticipant(creator);
        return eventRepository.save(event);
    }

    @Override
    public Event createEvent(User creator, Place place, Date startDate, Collection<User> invited, String message) {
        Event event = Event.builder()
                .creator(creator)
                .place(place)
                .eventDate(startDate)
                .creationDate(new Date())
                .message(message)
                .build();
        event.addParticipant(creator);
        event.addInvitedUsers(invited);
        return eventRepository.save(event);
    }

    @Override
    public Event inviteUser(Event event, User invited) {
        event.addInvitedUser(invited);
        return eventRepository.save(event);
    }

    @Override
    public Event inviteUsers(Event event, Collection<User> invitedUsers) {
        event.addInvitedUsers(invitedUsers);
        return eventRepository.save(event);
    }

    @Override
    public Event acceptInvitation(Event event, User acceptedUser) {
        event.removeInvitedUser(acceptedUser);
        event.addParticipant(acceptedUser);
        return eventRepository.save(event);
    }

    @Override
    public Event rejectInvitation(Event event, User rejectedUser) {
        event.removeInvitedUser(rejectedUser);
        return eventRepository.save(event);
    }

    @Override
    public Optional<Event> getEventById(String eventId) {
        return eventRepository.findById(eventId);
    }

    @Override
    public List<Event> getActualEventsByUser(User user) {
//        return eventRepository.findAllActualEventsByUserId(user.getUserId());
//
//        // КОСТЫЛЬ
        List<Event> all = eventRepository.findAll();
        List<Event> result = new ArrayList<>();
        for (Event event : all) {
            Set<User> participants = event.getParticipants();
            for (User participant : participants) {
                if (participant.getUserId().equals(user.getUserId()) && !result.contains(event)) {
                    result.add(event);
                }
            }
        }
        for (Event event : all) {
            Set<User> invited = event.getInvitedUsers();
            if (!CollectionUtils.isEmpty(invited)) {
                for (User invitedUser : invited) {
                    if (invitedUser.getUserId().equals(user.getUserId()) && !result.contains(event)) {
                        result.add(event);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Collection<Event> getActualAcceptedEventsByUser(User user) {
        return eventRepository.findAllAcceptedActualEventsByUserId(user.getUserId());
    }

    @Override
    public Collection<Event> getActualPendingEventsByUser(User user) {
        return eventRepository.findAllPendingActualEventsByUserId(user.getUserId());
    }

    @Override
    public Event changePlace(String eventId, Place place) {
        final Optional<Event> optional = eventRepository.findById(eventId);
        if (optional.isPresent()) {
            final Event event = optional.get();
            event.setPlace(place);
            updateEvent(event);
        }
        return null;
    }

    @Override
    public Event changeStartDate(String eventId, Date startDate) {
        final Optional<Event> optional = eventRepository.findById(eventId);
        if (optional.isPresent()) {
            final Event event = optional.get();
            event.setEventDate(startDate);
            updateEvent(event);
        }
        return null;
    }

    @Override
    public Event changeMessage(String eventId, String message) {
        final Optional<Event> optional = eventRepository.findById(eventId);
        if (optional.isPresent()) {
            final Event event = optional.get();
            event.setMessage(message);
            updateEvent(event);
        }
        return null;
    }

    @Override
    public Event updateEvent(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public void save(Event event) {
        eventRepository.save(event);
    }

}
