package com.nikitasex.stolovkascheduler.service.impl;

import com.nikitasex.stolovkascheduler.exception.StolovkaShedulerException;
import com.nikitasex.stolovkascheduler.model.Event;
import com.nikitasex.stolovkascheduler.model.Place;
import com.nikitasex.stolovkascheduler.model.Suggestion;
import com.nikitasex.stolovkascheduler.model.User;
import com.nikitasex.stolovkascheduler.repository.EventRepository;
import com.nikitasex.stolovkascheduler.repository.SuggestionRepository;
import com.nikitasex.stolovkascheduler.service.SuggestionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultSuggestionService implements SuggestionService {

    private final SuggestionRepository suggestionRepository;
    private final EventRepository eventRepository;

    @Override
    public Suggestion createSuggestion(User user, Event event, Date newEventDate, Place newPlace) {
        Suggestion suggestion = Suggestion.builder()
                .user(user)
                .event(event)
                .newEventDate(newEventDate)
                .newPlace(newPlace)
                .build();
        return suggestionRepository.save(suggestion);
    }

    @Override
    public Event acceptSuggestion(User user, Suggestion suggestion) {
        Event event = suggestion.getEvent();
        if (!event.getCreator().getUserId().equals(user.getUserId())) {
            throw new StolovkaShedulerException("You are not an event creator and you are not " +
                    "authorized to accept/reject suggestions");
        }
        if (suggestion.getNewEventDate() != null) {
            event.setEventDate(suggestion.getNewEventDate());
        }
        if (suggestion.getNewPlace() != null) {
            event.setPlace(suggestion.getNewPlace());
        }
        suggestionRepository.delete(suggestion);
        return eventRepository.save(event);
    }

    @Override
    public void rejectSuggestion(User user, Suggestion suggestion) {
        Event event = suggestion.getEvent();
        if (!event.getCreator().getUserId().equals(user.getUserId())) {
            throw new StolovkaShedulerException("You are not an event creator and you are not " +
                    "authorized to accept/reject suggestions");
        }
        suggestionRepository.delete(suggestion);
    }


}
