package com.nikitasex.stolovkascheduler.service.impl;

import com.nikitasex.stolovkascheduler.model.Feedback;
import com.nikitasex.stolovkascheduler.repository.FeedbackRepository;
import com.nikitasex.stolovkascheduler.service.FeedbackService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Default implementation of Feedback Service
 */
@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultFeedbackService implements FeedbackService {
    private FeedbackRepository feedbackRepository;

    @Override
    public Feedback setFeedback(String userId, String placeId, String feedbackText) {
        final Optional<Feedback> oldFeedback = getFeedbackByUserAndPlaceId(userId, placeId);
        if (oldFeedback.isPresent()) {
            Feedback updated = oldFeedback.get();
            updated.setFeedback(feedbackText);
            return feedbackRepository.save(updated);
        }
        Feedback created = Feedback.builder().userId(userId).placeId(placeId).feedback(feedbackText).build();
        return feedbackRepository.save(created);
    }

    @Override
    public Collection<Feedback> getFeedbacksByUserId(String userId) {
        return feedbackRepository.findAllByUserId(userId);
    }

    @Override
    public Collection<Feedback> getFeedbacksByPlaceId(String placeId) {
        return feedbackRepository.findAllByPlaceId(placeId);
    }

    @Override
    public Optional<Feedback> getFeedbackByUserAndPlaceId(String userId, String placeId) {
        return Optional.of(feedbackRepository.findByUserIdAndPlaceId(userId, placeId));
    }
}
