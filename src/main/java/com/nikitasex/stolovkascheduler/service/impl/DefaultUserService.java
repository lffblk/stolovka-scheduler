package com.nikitasex.stolovkascheduler.service.impl;

import com.nikitasex.stolovkascheduler.model.User;
import com.nikitasex.stolovkascheduler.repository.UserRepository;
import com.nikitasex.stolovkascheduler.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;


    @Override
    public User findUser(String userName, String firstName, String lastName) {
        if (userName != null) {
            return userRepository.findByUserName(userName);
        }
        else {
            return userRepository.findByFirstNameAndLastName(firstName, lastName);
        }
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }


    public List<String> getMentionedUserIds(Update update) {
        List<String> result = Arrays.stream(update.getMessage().getText().split(" "))
                .filter(s -> s.startsWith("@"))
                .map(s -> s.replaceFirst("@", ""))
                .map(s -> {
                    User u = userRepository.findByUserName(s);
                    return (u == null) ? null : u.getUserId();
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        result.addAll(update.getMessage().getEntities().stream()
                .filter(e -> e.getUser() != null)
                .map(e -> e.getUser().getId().toString())
                .collect(Collectors.toList()));

        return result;
    }
}
