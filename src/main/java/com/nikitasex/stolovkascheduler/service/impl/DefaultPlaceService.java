package com.nikitasex.stolovkascheduler.service.impl;

import com.google.maps.GeoApiContext;
import com.google.maps.NearbySearchRequest;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlaceType;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.PlacesSearchResult;
import com.nikitasex.stolovkascheduler.model.Place;
import com.nikitasex.stolovkascheduler.repository.PlaceRepository;
import com.nikitasex.stolovkascheduler.service.PlaceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * Default implementation of Place Service. Uses Google Place API to process places
 */
@Slf4j
@Service
public class DefaultPlaceService implements PlaceService {
    @Value("${places.api-key}")
    private String apiKey;
    private final PlaceRepository placeRepository;

    @Autowired
    public DefaultPlaceService(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Override
    public Optional<Place> getPlaceById(String placeId) {
        return placeRepository.findById(placeId);
    }

    @Override
    public List<Place> findNearbyByLocation(double latitude, double longitude) {
        final List<Place> places = new ArrayList<>();
        places.addAll(findNearbyByRequest(
                createNearbyRequest(latitude, longitude).type(PlaceType.RESTAURANT)));
        places.addAll(findNearbyByRequest(
                createNearbyRequest(latitude, longitude).type(PlaceType.CAFE)));
        places.addAll(findNearbyByRequest(
                createNearbyRequest(latitude, longitude).type(PlaceType.BAR)));
        placeRepository.saveAll(places);
        return places;
    }

    @Override
    public List<Place> findNearbyByLocationAndRadius(double latitude, double longitude, int radius) {
        final List<Place> places = new ArrayList<>();
        places.addAll(findNearbyByRequest(
                createNearbyRequest(latitude, longitude).type(PlaceType.RESTAURANT).radius(radius)));
        places.addAll(findNearbyByRequest(
                createNearbyRequest(latitude, longitude).type(PlaceType.CAFE).radius(radius)));
        places.addAll(findNearbyByRequest(
                createNearbyRequest(latitude, longitude).type(PlaceType.BAR).radius(radius)));
        placeRepository.saveAll(places);
        return places;
    }

    /**
     * Executes Nearby Search Request and parses results to collection of places
     *
     * @param request Neaby Search Request to execute
     * @return found places collection
     */
    private Collection<Place> findNearbyByRequest(NearbySearchRequest request) {
        PlacesSearchResponse response;
        try {
            response = request.await();
        } catch (ApiException | InterruptedException | IOException e) {
            log.debug("Error occurs during Nearby Place request execution", e);
            throw new RuntimeException(e);
        }
        return parseSearchResults(Objects.requireNonNull(response).results);
    }

    /**
     * Parses Google PlacesSearchResult array to collection of internal Places
     *
     * @param results Google PlaceSearchResult array
     * @return collection of internal Places
     */
    private Collection<Place> parseSearchResults(PlacesSearchResult... results) {
        if (ArrayUtils.isEmpty(results)) {
            return Collections.emptyList();
        }
        final Collection<Place> places = new ArrayList<>();
        for (PlacesSearchResult result : results) {
            places.add(Place.newBuilder().setId(result.placeId)
                    .setName(result.name)
                    .setRating(result.rating)
                    .setAddress(result.vicinity)
                    .setLastUpdated(new Date())
                    .build());
        }
        return places;
    }

    /**
     * Creates new Nearby Place search request by latitude, longitude. Sets default search radius as 1000 meters.
     *
     * @param latitude  latitude of nearby search center point
     * @param longitude longitude of nearby search center point
     * @return Nearby Search request to modify and execute
     */
    private NearbySearchRequest createNearbyRequest(double latitude, double longitude) {
        final GeoApiContext context = new GeoApiContext.Builder().apiKey(apiKey).build();
        return PlacesApi.nearbySearchQuery(context, new LatLng(latitude, longitude)).radius(1000);
    }
}
