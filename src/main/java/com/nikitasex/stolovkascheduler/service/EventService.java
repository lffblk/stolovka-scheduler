package com.nikitasex.stolovkascheduler.service;

import com.nikitasex.stolovkascheduler.model.Event;
import com.nikitasex.stolovkascheduler.model.Place;
import com.nikitasex.stolovkascheduler.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Provides methods to process Lunch Events
 */
public interface EventService {
    /**
     * Creates new Lunch event in specified Place and specified date
     *
     * @param creator   User that created the Lunch Event
     * @param place     Place of Lunch Event
     * @param startDate Start Date and Time of Lunch Event
     * @param message   Lunch event description
     * @return created event
     */
    Event createEvent(User creator, Place place, Date startDate, String message);

    /**
     * Creates new Lunch event in specified Place and specified Date and with specified Participants
     *
     * @param creator   User that created the Lunch Event
     * @param place     Place of Lunch Event
     * @param startDate Start Date and Time of Lunch Event
     * @param invited   Users that should be invited to the Lunch Event
     * @param message   Lunch event description
     * @return created event
     */
    Event createEvent(User creator, Place place, Date startDate, Collection<User> invited, String message);

    /**
     * Invites an user to the event
     *
     * @param event   Lunch event
     * @param invited Invited User
     * @return updated event
     */
    Event inviteUser(Event event, User invited);

    /**
     * Invites users collection to the event
     *
     * @param event        Lunch event
     * @param invitedUsers Invited Users collection
     * @return updated event
     */
    Event inviteUsers(Event event, Collection<User> invitedUsers);

    /**
     * Add invited user to Event participants list
     *
     * @param event        Lunch event
     * @param acceptedUser User that accepted event
     * @return updated event
     */
    Event acceptInvitation(Event event, User acceptedUser);

    /**
     * Remove invited user from the Event
     *
     * @param event        Lunch event
     * @param rejectedUser User that rejected event
     * @return updated event
     */
    Event rejectInvitation(Event event, User rejectedUser);

    /**
     * Returns Lunch Event by ID
     *
     * @param eventId Lunch event ID
     * @return Lunch Event
     */
    Optional<Event> getEventById(String eventId);

    /**
     * Returns all actual events related to the user (either accepted or pending)
     *
     * @param user user to get events
     * @return Actual events collection
     */
    List<Event> getActualEventsByUser(User user);

    /**
     * Returns all actual accepted events related to the user
     *
     * @param user user to get events
     * @return Actual accepted events collection
     */
    Collection<Event> getActualAcceptedEventsByUser(User user);

    /**
     * Returns all actual events related to the user, that are not accepted yet
     *
     * @param user user to get events
     * @return Actual pending events collection
     */
    Collection<Event> getActualPendingEventsByUser(User user);

    /**
     * Chenges Lunch event place
     *
     * @param eventId Lunch event ID to modify
     * @param place   New place
     * @return updated event
     */
    Event changePlace(String eventId, Place place);

    /**
     * Changes Lunch event start date
     *
     * @param eventId   Lunch event to modify
     * @param startDate New start date
     * @return updated event
     */
    Event changeStartDate(String eventId, Date startDate);

    /**
     * Changes Lunch event description
     *
     * @param eventId Lunch event to modify
     * @param message New Lunch event description
     * @return updated event
     */
    Event changeMessage(String eventId, String message);

    /**
     * Update event
     *
     * @param event event
     * @return updated event
     */
    Event updateEvent(Event event);

    /**
     * Saves Event in DB
     *
     * @param event creating or updating event
     */
    void save(Event event);
}
