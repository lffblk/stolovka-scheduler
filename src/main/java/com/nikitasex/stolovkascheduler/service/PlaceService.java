package com.nikitasex.stolovkascheduler.service;

import com.nikitasex.stolovkascheduler.model.Place;

import java.util.List;
import java.util.Optional;

/**
 * Provides methods to process Places (cafes, restaurants and so forth).
 */
public interface PlaceService {
    /**
     * Returns Place description by Place ID
     *
     * @param placeId Place ID
     * @return Place description
     */
    Optional<Place> getPlaceById(String placeId);

    /**
     * Returns Places in radius of 1000 meters from the defined point
     *
     * @param latitude  latitude of the center of search area
     * @param longitude longitude of the center of search area
     * @return found Places list
     */
    List<Place> findNearbyByLocation(double latitude, double longitude);

    /**
     * Returns Places in radius of 1000 meters from the defined point
     *
     * @param latitude  latitude of the center of search area
     * @param longitude longitude of the center of search area
     * @return found Places list
     */
    List<Place> findNearbyByLocationAndRadius(double latitude, double longitude, int radius);
}
