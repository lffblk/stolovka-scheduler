package com.nikitasex.stolovkascheduler.service;

import com.nikitasex.stolovkascheduler.model.Event;
import com.nikitasex.stolovkascheduler.model.Place;
import com.nikitasex.stolovkascheduler.model.Suggestion;
import com.nikitasex.stolovkascheduler.model.User;

import java.util.Date;

public interface SuggestionService {

    Suggestion createSuggestion(User user, Event event, Date newEventDate, Place newPlace);

    Event acceptSuggestion(User user, Suggestion suggestion);

    void rejectSuggestion(User user, Suggestion suggestion);
}
