package com.nikitasex.stolovkascheduler.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Describes Lunch event change suggestion.
 */
@Builder
@Data
@Document(collection = "suggestion")
@EqualsAndHashCode
public class Suggestion {
    @DBRef(db = "event")
    private Event event;

    private Date newEventDate;

    private Place newPlace;

    private User user;

    @Override
    public String toString() {
        return "User " + user.toString() + " suggested changes to event " + event.toString() + ":\n"
                + (newEventDate == null ? "" : "Date: " + newEventDate) + "\n"
                + (newPlace == null ? "" : "Place: " + newPlace);
    }
}
