package com.nikitasex.stolovkascheduler.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Defines a place - cafe, restaurant, bar
 */
@NoArgsConstructor
@Data
@Document(collection = "place")
public class Place {
    /**
     * Unique place identifier
     */
    @Id
    private String id;
    /**
     * Place name
     */
    private String name;
    /**
     * Google place rating. Can be outdated, should be updated periodically
     */
    private float googleRating;
    /**
     * Place address
     */
    private String address;
    /**
     * Last date of entity modification. Will be used for updating later
     */
    private Date lastUpdated;

    @Override
    public String toString() {
        return name + "; " + address + "\nRating: " + googleRating;
    }

    public static Builder newBuilder() {
        return new Place().new Builder();
    }

    public class Builder {
        /**
         * Private intentionally. Should be invoked from Place static method only
         */
        private Builder() {
            //Should not be executed outside
        }

        /**
         * Sets place ID
         *
         * @param id Place ID
         * @return Builder with defined place ID
         */
        public Builder setId(String id) {
            Place.this.id = id;
            return this;
        }

        /**
         * Sets place name
         *
         * @param name Place name
         * @return Builder with defined place name
         */
        public Builder setName(String name) {
            Place.this.name = name;
            return this;
        }

        /**
         * Sets place rating
         *
         * @param rating Place rating
         * @return Builder with defined place rating
         */
        public Builder setRating(float rating) {
            Place.this.googleRating = rating;
            return this;
        }

        /**
         * Sets place address
         *
         * @param address Place address
         * @return Builder with defined place address
         */
        public Builder setAddress(String address) {
            Place.this.address = address;
            return this;
        }

        /**
         * Sets place date of last update
         *
         * @param lastUpdated Place date of last update
         * @return Builder with defined last updated date
         */
        public Builder setLastUpdated(Date lastUpdated) {
            Place.this.lastUpdated = lastUpdated;
            return this;
        }

        /**
         * Creates new place with data filled on the builder
         *
         * @return new Place
         */
        public Place build() {
            if (lastUpdated == null) {
                lastUpdated = new Date();
            }
            return Place.this;
        }
    }
}
