package com.nikitasex.stolovkascheduler.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Contains User Feedback for specified Place
 */
@Builder
@Data
@Document(collection = "feedback")
public class Feedback {
    /**
     * Feedback id
     */
    @Id
    private String id;
    /**
     * Id of User who left the feedback
     */
    private String userId;

    /**
     * Id of Place described in the feedback
     */
    private String placeId;

    /**
     * The feedback itself
     */
    private String feedback;
}
