package com.nikitasex.stolovkascheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nikitasex.stolovkascheduler.util.StaticBeanService;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Describes Lunch event.
 */
@Builder
@Data
@Document(collection = "event")
@EqualsAndHashCode
public class Event {
    /**
     * Unique Event identifier
     */
    @Id
    private String id;
    /**
     * Event creation date
     */
    private Date creationDate;
    /**
     * Event place
     */
    private Place place;
    /**
     * Event start date
     */
    private Date eventDate;
    /**
     * Event message
     */
    private String message;
    /**
     * Event creator
     */
    private User creator;
    /**
     * Event participants
     */
    private Set<User> participants;
    /**
     * Users invited to the event (and invitation is not rejected or accepted yet)
     */
    private Set<User> invitedUsers;

    /**
     * Adds new participant to the event
     *
     * @param participant New participant
     */
    public void addParticipant(User participant) {
        if (participants == null) {
            participants = new HashSet<>();
        }
        for (User user : participants) {
            if (user.getUserId().equals(participant.getUserId())) {
                return;
            }
        }
        participants.add(participant);
    }

    /**
     * Add collection of participants to the event
     *
     * @param newParticipants New participants collection
     */
    public void addParticipants(Collection<User> newParticipants) {
        if (participants == null) {
            participants = new HashSet<>();
        }
        participants.addAll(newParticipants);
    }

    /**
     * Removes the participant from the event
     *
     * @param participant Participant to be removed
     */
    public void removeParticipant(User participant) {
        if (creator.equals(participant) || CollectionUtils.isEmpty(participants)) {
            return;
        }
        participants.remove(participant);
    }

    /**
     * Adds invited user to the Event
     *
     * @param user User invited to the event
     */
    public void addInvitedUser(User user) {
        if (invitedUsers == null) {
            invitedUsers = new HashSet<>();
        }
        if (participants == null) {
            participants = new HashSet<>();
        }
        for (User participant : participants) {
            if (user.getUserId().equals(participant.getUserId())) {
                return;
            }
        }
        invitedUsers.add(user);
    }

    /**
     * Adds invited users collection to the Event
     *
     * @param users Invited to the event users collection
     */
    public void addInvitedUsers(Collection<User> users) {
        users.forEach(this::addInvitedUser);
    }

    /**
     * Remove invited user from the Event
     *
     * @param user Invited user to be removed
     */
    public void removeInvitedUser(User user) {
        if (CollectionUtils.isEmpty(invitedUsers)) {
            return;
        }
        invitedUsers.remove(user);
    }

    @JsonIgnore
    public String getEventDateStr() {
        DateFormat format = StaticBeanService.getBean(DateFormat.class);
        return format.format(eventDate);
    }

    @Override
    public String toString() {
        return getEventDateStr() + " " + place.getName() + "\n" + message;
    }
}
