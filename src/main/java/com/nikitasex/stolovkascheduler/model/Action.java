package com.nikitasex.stolovkascheduler.model;

import com.nikitasex.stolovkascheduler.exception.StolovkaShedulerException;
import com.nikitasex.stolovkascheduler.repository.EventRepository;
import com.nikitasex.stolovkascheduler.repository.UserRepository;
import com.nikitasex.stolovkascheduler.service.EventService;
import com.nikitasex.stolovkascheduler.service.PlaceService;
import com.nikitasex.stolovkascheduler.service.SuggestionService;
import com.nikitasex.stolovkascheduler.service.UserService;
import com.nikitasex.stolovkascheduler.util.StaticBeanService;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

public enum Action {

    PLACES("/places", "/places optional_search_radius",
            "Finds places in specific radius (meters) around you. Bot should know your location. " +
                    "Default radius = 500 meters.") {
        @Override
        public String getResult(Update update, User user) {
            PlaceService placeService = StaticBeanService.getBean(PlaceService.class);
            UserService userService = StaticBeanService.getBean(UserService.class);
            String[] splitted = getSplittedMesage(update);

            Location location = user.getLocation();
            if (location == null) {
                return "Please send your location";
            }
            int radius;
            try {
                radius = splitted.length < 2 ? 500 : Integer.parseInt(splitted[1]);
            } catch (NumberFormatException e) {
                return "Please set correct radius";
            }

            List<Place> nearbyByLocationAndRadius = placeService
                    .findNearbyByLocationAndRadius(location.getLatitude(), location.getLongitude(), radius);
            user.setLastPlaces(nearbyByLocationAndRadius);
            userService.save(user);

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < nearbyByLocationAndRadius.size(); i++) {
                Place place = nearbyByLocationAndRadius.get(i);
                result
                        .append(i + 1)
                        .append(". ")
                        .append(place.getName())
                        .append(" ")
                        .append(place.getGoogleRating())
                        .append("\n");
            }
            return result.toString();
        }
    },
    PLACE("/place", "/place place_num",
            "Returns additional information for specified place. Place num is an index of your last places search") {
        @Override
        public String getResult(Update update, User user) {
            String[] splitted = Action.getSplittedMesage(update);
            if (splitted.length < 2) {
                return "Please enter number of place you are interested in.\nCommand pattern: " +
                        this.expectedPattern;
            }
            if (user.getLastPlaces() == null || user.getLastPlaces().isEmpty()) {
                return "Please find nearest places before.\nCommand pattern: "
                        + PLACES.expectedPattern;
            }
            try {
                return getLastUserPlaceByNum(user, splitted[1]).toString();
            } catch (StolovkaShedulerException e) {
                return e.getMessage();
            }
        }
    },
    CREATE("/create", "/create place_num time message",
            "Creates new Lunch event in specified place, on specified time and with specified message. " +
                    "Place num is an index of your last places search") {
        @Override
        public String getResult(Update update, User user) {
            String[] splitted = Action.getSplittedMesage(update);
            DateFormat dateFormat = StaticBeanService.getBean(DateFormat.class);
            EventService eventService = StaticBeanService.getBean(EventService.class);

            String errorMsg = "Incorrect command.\nCommand pattern: " + this.expectedPattern;
            if (splitted.length < 4) {
                return errorMsg;
            }
            try {
                Date date = dateFormat.parse(splitted[2]);
                Place place = getLastUserPlaceByNum(user, splitted[1]);
                String msgText = update.getMessage().getText();
                String description = msgText.substring(msgText.indexOf('\'') + 1, msgText.lastIndexOf('\''));
                Event event = eventService.createEvent(user, place, date, description);
                return event.toString();
            } catch (StolovkaShedulerException | ParseException e) {
                return e.getMessage() + "\n" + errorMsg;
            }
        }
    },
    EVENTS("/events", "/events", "Returns all Lunch events associated with you") {
        @Override
        public String getResult(Update update, User user) {
            EventService eventService = StaticBeanService.getBean(EventService.class);

            List<Event> actualEventsByUser = eventService.getActualEventsByUser(user);
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < actualEventsByUser.size(); i++) {
                Event event = actualEventsByUser.get(i);

                StringBuilder invitedUsersSB = new StringBuilder();
                if (event.getInvitedUsers() != null) {
                    for (User invitedUser : event.getInvitedUsers()) {
                        invitedUsersSB
                                .append("@")
                                .append(invitedUser.toString())
                                .append("\n");
                    }
                }

                StringBuilder participantsSB = new StringBuilder();
                if (event.getParticipants() != null) {
                    for (User participant : event.getParticipants()) {
                        participantsSB
                                .append("@")
                                .append(participant.toString())
                                .append("\n");
                    }
                }

                result
                        .append(i + 1)
                        .append(". ")
                        .append(event.getEventDateStr())
                        .append(" ")
                        .append(event.getPlace().getName())
                        .append("; ")
                        .append(event.getPlace().getAddress())
                        .append("\nInvited:\n")
                        .append(invitedUsersSB.toString())
                        .append("\nAccepted:\n")
                        .append(participantsSB.toString())
                        .append("\n\n");
            }
            String resultStr = result.toString();
            return resultStr.isEmpty() ? "There is no events associated with you" : resultStr;
        }
    },
    INVITE("/invite", "/invite event_num users_to_invite...",
            "Invites Telegram users to the Lunch event. Event num is the index of last events search") {
        @Override
        public String getResult(Update update, User user) {
            return Action.performUserEventOperation("/invite", update, user, Action::inviteUserToEvent);
        }
    },
    ACCEPT("/accept", "/accept event_num",
            "Accepts invitation to the Lunch event. Event num is the index of last events search") {
        @Override
        public String getResult(Update update, User user) {
            String[] splitted = Action.getSplittedMesage(update);
            if (splitted.length < 2) {
                return "Invalid command.\nCommand pattern: " + this.expectedPattern;
            }
            Event event = Action.getLastUserEventByNum(user, splitted[1]);
            Action.acceptEventByUser(user.getUserId(), event);
            return "Operation successful";
        }
    },
    REJECT("/reject", "/reject event_num",
            "Rejects invitation to the Lunch event. Event num is the index of last events search") {
        @Override
        public String getResult(Update update, User user) {
            String[] splitted = Action.getSplittedMesage(update);
            if (splitted.length < 2) {
                return "Invalid command.\nCommand pattern: " + this.expectedPattern;
            }
            Event event = Action.getLastUserEventByNum(user, splitted[1]);
            Action.rejectEventByUser(user.getUserId(), event);
            return "Operation successful";
        }
    },
    SUGGEST("/suggest", "/suggest event_num new_time new_place_num",
            "Suggests new time or place for Lunch event. event num is the index of last events search") {
        @Override
        public String getResult(Update update, User user) {
            String[] splitted = Action.getSplittedMesage(update);
            DateFormat dateFormat = StaticBeanService.getBean(DateFormat.class);
            SuggestionService suggestionService = StaticBeanService.getBean(SuggestionService.class);

            String errorMsg = "Incorrect command.\nCommand pattern: " + this.expectedPattern;
            if (splitted.length < 4) {
                return errorMsg;
            }
            try {
                Date date = dateFormat.parse(splitted[2]);
                Event event = getLastUserEventByNum(user, splitted[1]);
                Place place = getLastUserPlaceByNum(user, splitted[3]);
                Suggestion suggestion = suggestionService.createSuggestion(user, event, date, place);
                return suggestion.toString();
            } catch (StolovkaShedulerException | ParseException e) {
                return e.getMessage() + "\n" + errorMsg;
            }
        }
    },
    HELP("/help", "/help", "Provides information for every bot command") {
        @Override
        public String getResult(Update update, User user) {
            StringBuilder builder = new StringBuilder("Stolovka Scheduler bot commands:");
            for (Action action : values()) {
                builder.append('\n')
                        .append(action.command)
                        .append(": Expected pattern ")
                        .append(action.expectedPattern)
                        .append(". ")
                        .append(action.description);
            }
            return builder.toString();
        }
    };

    private String command;
    String expectedPattern;
    private String description;

    Action(String command, String expectedPattern, String description) {
        this.command = command;
        this.expectedPattern = expectedPattern;
        this.description = description;
    }

    public String getCommand() {
        return command;
    }

    public abstract String getResult(Update update, User user);

    public static Action getActionByCommand(String command) {
        for (Action action : Action.values()) {
            if (action.command.equals(command)) {
                return action;
            }
        }
        throw new UnsupportedOperationException("Unknown command: " + command);
    }

    private static String[] getSplittedMesage(Update update) {
        Message message = update.getMessage();
        return message.getText().split(" ");
    }

    private static Place getLastUserPlaceByNum(User user, String placeNumStr) {
        int placeNum;
        try {
            placeNum = Integer.parseInt(placeNumStr);
            return user.getLastPlaces().get(placeNum - 1);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            throw new StolovkaShedulerException("Please send correct place number");
        }
    }

    public static Event getLastUserEventByNum(User user, String eventNumStr) {
        int eventNum;
        try {
            EventService eventService = StaticBeanService.getBean(EventService.class);
            List<Event> actualEventsByUser = eventService.getActualEventsByUser(user);

            eventNum = Integer.parseInt(eventNumStr);
            return actualEventsByUser.get(eventNum - 1);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            throw new StolovkaShedulerException("Please send correct place number");
        }
    }

    private static void inviteUserToEvent(String userId, Event event) {
        UserRepository userRepository = StaticBeanService.getBean(UserRepository.class);
        Optional<User> invitedUser = userRepository.findById(userId);
        invitedUser.ifPresent(event::addInvitedUser);
    }

    private static void acceptEventByUser(String userId, Event event) {
        UserRepository userRepository = StaticBeanService.getBean(UserRepository.class);
        EventRepository eventRepository = StaticBeanService.getBean(EventRepository.class);
        Optional<User> acceptedUser = userRepository.findById(userId);
        acceptedUser.ifPresent(event::removeInvitedUser);
        acceptedUser.ifPresent(event::addParticipant);
        eventRepository.save(event);
    }

    private static void rejectEventByUser(String userId, Event event) {
        UserRepository userRepository = StaticBeanService.getBean(UserRepository.class);
        EventRepository eventRepository = StaticBeanService.getBean(EventRepository.class);
        Optional<User> rejectedUser = userRepository.findById(userId);
        rejectedUser.ifPresent(event::removeInvitedUser);
        eventRepository.save(event);
    }

    private static String performUserEventOperation(String operation,
                                                    Update update,
                                                    User user,
                                                    BiConsumer<String, Event> eventFunction) {
        String[] splitted = Action.getSplittedMesage(update);
        if (splitted.length < 2) {
            return "Invalid command.\nCommand pattern: " + operation + " event_num";
        }
        EventService eventService = StaticBeanService.getBean(EventService.class);
        UserService userService = StaticBeanService.getBean(UserService.class);
        int eventNum = Integer.parseInt(splitted[1]);
        List<Event> actualEventsByUser = eventService.getActualEventsByUser(user);
        Event event = actualEventsByUser.get(eventNum - 1);

        userService.getMentionedUserIds(update).forEach(id -> eventFunction.accept(id, event));
        eventService.save(event);

        return "/invite".equals(operation)
                ? "You are invited by\n@" + user.toString() + "\n\nto following event:\n" + event.toString()
                : operation + " successful";
    }
}
