package com.nikitasex.stolovkascheduler.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.telegram.telegrambots.meta.api.objects.Location;

import java.util.List;

@Data
@Builder
@Document(collection = "user")
@EqualsAndHashCode
public class User {
    @Id
    private String userId;
    private String userName;
    private String firstName;
    private String lastName;
    private boolean isBot;
    private Location location;
    @DBRef(db = "place")
    private List<Place> lastPlaces;

    @Override
    public String toString() {
        return getUserName() == null ? getFirstName() + " " + getLastName() : getUserName();
    }
}
