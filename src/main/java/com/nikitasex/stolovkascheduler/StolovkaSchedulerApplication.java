package com.nikitasex.stolovkascheduler;

import com.nikitasex.stolovkascheduler.bot.StolovkaSchedulerTelegramBot;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
@SpringBootApplication
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class StolovkaSchedulerApplication implements ApplicationRunner {

    private final StolovkaSchedulerTelegramBot bot;

    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(StolovkaSchedulerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(bot);
        }
        catch (TelegramApiException e) {
            log.error("Exception: ", e);
            throw new IllegalStateException(e);
        }
    }
}
