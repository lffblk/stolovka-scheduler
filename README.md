# stolovka-scheduler

Actors:
1. User
2. Event Participant (extends User)
3. Event Creator (extends Event Participant)
4. System
5. Places (cafes) Management

User Stories:
1. As a User I want to register in system
2. As a User I want to see the list of nearest places (cafes) by geolocation and radius
3. As a User I want to see details (address, distance, rating, feedbacks) of particular place
4. As a User I want to evaluate particular place
5. As a User I want to leave a feedback for particular place
6. As a Event Creator I want to create new Lunch event in particular place in specified time
7. As a Event Participant I want to invite friends to join created event
8. As a Event Participant I want to recieve notification about event wich I invited to
9. As a Event Participant I want to see event details (event, place, time, invited users, users accepted event, users rejected event)
10. As a Event Participant I want to accept / reject the event
11. As a Event Participant I want to suggest changes in event (time and / or place) with comment
12. As a Event Participant I want to recieve notification about all suggestions from other participants
13. As a Event Creator I want to change the event
14. As a Event Participant I want to recieve notification about all changes in event
15. As a System I want to store registered users data, places rating and feedback, events and suggestions
16. As a System I want to prevent passed events modifications / suggestions / invitations
17. As a Places (cafes) Management I want to pay ExNikitas big money to move my placy upper in the nearest places list

DB:
* Users (telegram user name, telegram user id)
* Places (place id [from Google or generated], rating)
* PlaceFeedbacks (telegram user id, comment)
* Event (id, name, creation date, place, time, message, creator)
* Suggestion (id, user id, place, time, comment)

Telegram commands:
1. /hello - register
2. /places - nearest places list. Should be ability to specify geolocation and radius. List should contain place id and place name.
3. /place + id - place details
4. /evaluate + place id + mark - evaluate place
5. /feedback + place id + message - leave feedback
6. /create + place id + name + time + message - create new event
7. /event + event id - event details
8. /invite + event id + telegram user names - invite users to event
9. /accept + event id - accept the event
10. /reject + event id - reject the event
11. /suggest + event id + time + place + message - suggest changes
12. /update + event id + place id + name + time + message - update existing event. Only creator have access to this operation